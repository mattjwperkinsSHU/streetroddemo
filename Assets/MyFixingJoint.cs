﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyFixingJoint : MonoBehaviour {

	public SpriteRenderer SRIndicator;
	bool bIsInPlace = false;
	bool bIsScrewingIn = true;
	public bool bIsFixed = false;

	public GameObject Holder;
	public Draggable DragHolder;

	int depth = 0;
	// Use this for initialization
	void Start () {
		SRIndicator = GetComponent<SpriteRenderer>();
		Holder = transform.parent.gameObject;
		DragHolder = Holder.GetComponent<Draggable>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		bIsInPlace = true;
		SRIndicator.color = Color.green;
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		bIsInPlace = false;
		SRIndicator.color = Color.yellow;
	}

	private void OnMouseDown()
	{
		if (!bIsInPlace) return;

		if (bIsScrewingIn)
		{
			if (depth == 0){
				depth = 1;
				SRIndicator.color = Color.cyan;
				bIsFixed = true;
				DragHolder.CheckFixings();
			}
			else if (depth == 1)
			{
				depth = 2;
				bIsScrewingIn = false;
				SRIndicator.color = Color.blue;
			}
		}
		else
		{
			if (depth == 2)
			{
				depth = 1;
				SRIndicator.color = Color.cyan;
			}
			else if (depth == 1)
			{
				depth = 0;
				bIsScrewingIn = true;
				SRIndicator.color = Color.green;
				bIsFixed = false;
				DragHolder.CheckFixings();
			}
		}
	}
}
