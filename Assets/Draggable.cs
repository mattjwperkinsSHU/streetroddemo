﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour{

	bool bIsDragging = false;

	public bool bIsFixed = false;

	public MyFixingJoint[] Fixings;

	Camera MC;
	// Use this for initialization
	void Start () {
		MC = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		if (bIsDragging)
        {
			Vector2 mousepos;
			mousepos = Input.mousePosition;
			mousepos = MC.ScreenToWorldPoint(mousepos);

			this.transform.position = mousepos;
		}
	}

	public void OnMouseDown()
	{
		if (!bIsFixed)
		{
			bIsDragging = true;
		}
	}

	public void OnMouseUp()
	{
		if (!bIsFixed)
		{
			bIsDragging = false;
		}
	}

	public void CheckFixings(){

		bool isStuckInPlace = false;

		foreach (MyFixingJoint Joint in Fixings)
		{
			if (Joint.bIsFixed) isStuckInPlace = true;	
		}

		bIsFixed = isStuckInPlace;
	}
}
